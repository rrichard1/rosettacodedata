#!/bin/bash
SCRIPTDIR="$( dirname "${BASH_SOURCE[0]}" )"

source "$SCRIPTDIR/preamble"

# compiler and language
OPERATION='compile'
COMPBIN=javac
OPTIMIZATION=''
LANGEXT=(java)
std="default"

# operations to be done on every input source file
start_process_sourcefile()
{
	 mainfound='no'
	 classfound='yes'
}

next_process_sourcefile()
{
	 # get only the first occurrence (http://sed.sourceforge.net/sedfaq4.html)
	 # first try with "public class"
	 classname=`"$SEDBIN" -n '0,/.*\s*public\s\+class\s\+\([a-zA-Z0-9_-]\+\).*$/s//\1/p' "$1"`
	 # if not found, try looking for just class
	 if [ -z ${classname+x} -o "$classname" == "" ]; then 
		  classname=`"$SEDBIN" -n '0,/.*\s*class\s\+\([a-zA-Z0-9_-]\+\).*$/s//\1/p' "$1"`
	 fi
	 # if not found again, try looking for an interface (Java 8)
	 if [ -z ${classname+x} -o "$classname" == "" ]; then 
		  classname=`"$SEDBIN" -n '0,/.*\s*interface\s\+\([a-zA-Z0-9_-]\+\).*$/s//\1/p' "$1"`
	 fi
	 # if not found again, try looking for an enum
	 if [ -z ${classname+x} -o "$classname" == "" ]; then 
		  classname=`"$SEDBIN" -n '0,/.*\s*enum\s\+\([a-zA-Z0-9_-]\+\).*$/s//\1/p' "$1"`
	 fi
	 # check if a main method exists
	 if [[ "$1" == *.java ]]; then
		  $EGREPBIN -q "public\s+static\s+void\s+main\s*\(" "$1"
		  if [ $? -eq 0 ]; then
				# class with main found
				mainfound='yes'
		  fi
	 fi
	 if [ ! -z ${classname+x} -a "$classname" != "" ]; then # if set
		  mv "$1" "$classname.java"
    elif [[ "$1" == *.java ]]; then
		  # at least one java file has no class
		  classfound='no'
	 fi
}

done_process_sourcefile()
{
	 add_logentry "mainfound:$mainfound"
	 add_logentry "classfound:$classfound"
}

source "$SCRIPTDIR/read_args"

source "$SCRIPTDIR/source_setup"

# have to call "ls" again or "javac" doesn't interpret "$infiles" correctly
COMPILESTR="$COMPBIN `ls *.java`"
if [ ! -z ${CMPOPTS+x} ]; then
	 COMPILESTR="$COMPBIN ${CMPOPTS[@]} `ls *.java`"
fi

std='java8'
source "$SCRIPTDIR/compilation"

$TIMEBIN -o "$log" -f "$fstr" sh -c "$COMPILESTR" > "$stdout" 2> "$stderr"

add_logentry "std:$std"

noutfiles=`ls *.class 2> /dev/null | wc -l`
outfiles=`ls *.class 2> /dev/null`
if [ "$noutfiles" -gt 0 ]; then
	 # using "$outfiles" leaves '\n' in, which confuses 'du'
	 size=`$DUBIN -c *.class | awk '/total$/{print $1}'`
else
	 size=-1
fi
add_logentry "size:$size"

source "$SCRIPTDIR/logging"
