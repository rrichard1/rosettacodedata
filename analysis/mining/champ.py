#! /usr/bin/python

import yaml
import os.path
import subprocess
import collections
import argparse
import pickle



class RosettaCodeData:

    def __init__(self, basepath, task_file='Task.yaml', lang_file='Lang.yaml', meta='Meta'):
        """
        Read data in 'basepath', under which:
           'meta'/'task_file' is the yaml list of tasks
           'meta'/'lang_file' is the yaml list of languages
        """
        self.basepath = basepath
        self.task_file = os.path.join(self.basepath, meta, task_file)
        self.lang_file = os.path.join(self.basepath, meta, lang_file)
        self.read_taskfile()
        self.read_langfile()

    def read_taskfile(self):
        with open(self.task_file, 'r') as f:
            self.tasks = collections.OrderedDict(sorted(yaml.load(f).items()))

    def read_langfile(self):
        with open(self.lang_file, 'r') as f:
            self.langs = collections.OrderedDict(sorted(yaml.load(f).items()))

    def get_dir(self, lang, task):
        """Just the dir"""
        langpath = self.langs[lang]['path']
        taskpath = self.tasks[task]['path']
        path = os.path.join(self.basepath, 'Task', taskpath, langpath)
        return path

    def get_file(self, lang, task):
        """Just the base filename"""
        ext = self.langs[lang]['ext']
        name = self.tasks[task]['path'].lower()
        return name + "." + ext

    def get_path(self, lang, task):
        """Full path"""
        return os.path.join(self.get_dir(lang, task), self.get_file(lang, task))

    def has(self, lang, task):
        """Does task for lang exist?"""
        return lang in self.langs and task in self.tasks

    def get_task(self, lang, path):
        """If a task for path and lang exists return it, otherwise None"""
        for task in self.tasks:
            if self.tasks[task]['path'] == path:
                return task
        return None



def parse_notes(rc_data, filename, default_tag='notes', script_names={}):
    notes = {}
    with open(filename, 'r') as fp:
        for line in fp:
            if not line.startswith('#'):  # skip comments
                parts = line.split(':')
                # processing the last part (the actual note content)
                content = parts[-1] 
                content = [c.strip() for c in content.split(';')]
                content_dict = {}
                nc = 0
                for c in content:
                    nc += 1
                    sc = c.split(',')
                    if len(sc) == 1:
                        tag = default_tag + (str(nc) if nc > 1 else '')
                        newcont = [sc[0].strip()]
                    else:
                        tag = sc[0].strip()
                        newcont = [nc.strip() for nc in sc[1:]]
                    if not content_dict.has_key(tag):
                        content_dict[tag] = newcont
                    else:
                        content_dict[tag] = content_dict[tag] + newcont
                # processing the first parts (a reference to a task and language)
                ref = parts[:-1] # the rest is a reference to a task and language
                if len(ref) == 1:
                    # trying as a reference to file
                    sref = ref[0].strip()
                    langroot, fname = os.path.split(sref)
                    taskroot, lang = os.path.split(langroot)
                    root, path = os.path.split(taskroot)
                    task = rc_data.get_task(lang, path)
                elif len(ref) >= 3:
                    task = ref[0].strip()
                    lang = ref[1].strip()
                    fname = ref[2].strip()
                else:
                    # I don't know how to interpret the record
                    continue
                # reverse lookup from script names (also directory names) to full language names
                rev_script_names = [k for k, v in script_names.iteritems() if v == lang]
                lang = rev_script_names[0] if len(rev_script_names) == 1 else lang
                if rc_data.has(lang, task):
                    key = (task, lang, fname)
                    notes[key] = content_dict
                else:
                    print line
                    print >> sys.stderr, "error: parsing notes for", task, lang
    return notes
                


def todo(todo_file, keylist, start_at_line=1, true_key='x'):
    """
    Return the operations to do from file 'todo_file'.
    The result is encoded as an ordered dictionary: task name --> set of operations.
    @param keylist           a list of operations keys (the first column is the task name)
    @param start_at_list     the first line to be read in 'todo_file' (numbering starts at 1)
    @param true_key          the content interpreted as "include this operation"
    """
    import csv
    todo = collections.OrderedDict()
    with open(todo_file, 'r') as csv_fp:
        csvreader = csv.DictReader(csv_fp, fieldnames=('task',) + keylist, restkey='more')
        for entry in csvreader:
            if csvreader.line_num < start_at_line:
                continue
            cur_task = entry['task']
            todo[cur_task] = set([k for k in keylist if entry[k] == true_key])
    return todo



def mine(tasks, languages, lang_exts, rc_data, operation,
         report_dir='report', report_ext='log', ignore=[], extra_notes = None, \
         force=False, ignore_make=False, patch=False, \
         scripts_location='.', scripts_names={}, verbose=True):
    """
    Return the statistics about 'operation' for a list of 'tasks' and 'languages'.
    The result is an ordered dictionary: task name --> language --> filename --> stats,
    where 'stats' too is a dictionary with script-dependent statistics.
    @param languages       a list of languages to be processed
    @param lang_exts       a map: language --> file extension (without '.')
    @param rc_data         RosettaCodeData object to access the data
    @param operation       the identifier of the operation: 'loc', 'compile', or 'run'
    @param report_dir      subdirectory where the report files are written
    @param report_ext      extension (without '.') of report files
    @param ignore          list of files (absolute paths) to be skipped
    @param extra_notes     a dictionary (task, lang, fname) --> additional_stats that is added to stats
    @param force           generate again reports already present?
    @param ignore_make     do not use makefiles even if they are available?
    @param patch           apply patches (.patch files) when they are available?
    @param scripts_location location of operation scripts for the operation (named language_operation)
                            scripts must support the following CLI input arguments:
                              script [-d outdir] [-e ATTRIBUTE:VALUE] [-o OUTFILE] INFILES
    @param scripts_names   mapping language name --> prefix script names
    @param verbose         print information about what is being done?
    """
    import sys
    result = collections.OrderedDict()
    for task in tasks:
        result[task] = {}
        for lang in languages:
            result[task][lang] = collections.OrderedDict()
            ext = lang_exts[lang]
            if scripts_names.has_key(lang):
                script_name_pre = scripts_names[lang]
            else:
                script_name_pre = lang
            script = os.path.abspath(os.path.join(scripts_location, script_name_pre + '_' + operation))
            if not os.path.exists(script):
                print >> sys.stderr, "error: script " + script + " not found."
                return 1
            try:
                d = rc_data.get_dir(lang, task)
            except:
                d = None
            if d is None or not os.path.exists(d):
                if verbose:
                    print "warning: directory for task " + task + " in language " + lang + " not found."
                continue
            # switch to absolute path
            d = os.path.abspath(d)
            out_d = os.path.join(d, report_dir)
            if not os.path.exists(out_d):
                os.mkdir(out_d)
            # directory listing
            ls = [f for f in os.listdir(d) if \
                  os.path.isfile(os.path.join(d, f)) and \
                  os.path.join(d, f) not in ignore]
            if 'Makefile' in ls and not ignore_make:
                # call Makefile passing a reference to script
                if verbose:
                    print "operation: " + operation + \
                          " calling make on task " + task + " in language " + lang
                force_opt = ["-B"] if force else []
                verbose_opt = ["-s"] if not verbose else []
                subprocess.call(['make'] +
                                force_opt + verbose_opt + 
                                ['--directory=' + d, 
                                 'COMP=' + script,
                                 'REPORT=' + report_dir, 
                                 'OPERATION=' + operation])
            else:
                # Makefile not present or ignored: process directly each program file
                nv = 0
                for fn in sorted([f for f in ls if os.path.splitext(f)[1] == '.' + ext]):
                    nv += 1
                    target = os.path.join(d, fn)
                    if verbose:
                        print "operation: " + operation + \
                              " processing task " + task + " in language " + lang + " (file: " + fn + ")"
                    if force or not os.path.exists(target + '.' + operation):
                        nopatch_opt = ['-n'] if not patch else []
                        args = [script] + \
                               ['-d', report_dir] + \
                               ['-e', 'make:none'] + \
                               nopatch_opt + \
                               ['-o', target] + \
                               [target]
                        subprocess.call(args)
            # read in all .log reports for the task, regardless of how they've been generated
            ls_report = [f for f in os.listdir(out_d) if os.path.isfile(os.path.join(out_d, f))]
            for fn in sorted([f for f in ls_report if \
                              os.path.splitext(os.path.splitext(f)[0])[1] == '.' + operation and
                              os.path.splitext(f)[1] == '.' + report_ext]):
                logfile = os.path.join(out_d, fn)
                with open(logfile, 'r') as t:
                    loaded = yaml.load(t)
                    if len(loaded.keys()) == 0:
                        print >> sys.stderr, "error: log file " + logfile + " has no keys"
                        continue
                    if len(loaded.keys()) > 1:
                        if verbose:
                            print "warning: log file " + logfile + \
                                  " has multiple keys: reading the first one"
                    result[task][lang][logfile] = loaded[loaded.keys()[0]]
                    # extra key for target filename stored
                    result[task][lang][logfile]['target_fname'] = loaded.keys()[0]
                    # if available, add extra notes
                    if extra_notes is not None:
                        if (task, lang, fn) in extra_notes:
                            nkey = (task, lang, fn)
                        else:
                            nkey = None
                            root, ext = os.path.splitext(fn)
                            while ext != '' and (task, lang, root) not in extra_notes:
                                root, ext = os.path.splitext(root)
                            if (task, lang, root) in extra_notes:
                                nkey = (task, lang, root)
                        noval = False
                        if nkey is None:  # use first key
                            nkey = extra_notes.keys()[0]
                            noval = True
                        for k, v in extra_notes[nkey].iteritems():
                            result[task][lang][logfile][k] = ', '.join(v) if not noval else ''
    return result



def blob(fname, action, *args, **kwargs):
    """
    If a pickled file 'fname' exists, unpickle it and return it.
    Otherwise, run action(*args, **kwargs), pickle the result into file 'fname', and return it.
    """
    import pickle
    if os.path.exists(fname):
        with open(fname, 'rb') as blob:
            result = pickle.load(blob)
    else:
        if action is not None:
            result = action(*args, **kwargs)
            with open(fname, 'wb') as blob:
                pickle.dump(result, blob)
    return result



if __name__ == '__main__':
    from champ_rc import *
    import sys
    parser = argparse.ArgumentParser(description='Analyze Rosetta Code files.')
    parser.add_argument('--ignore', dest='ignore_file', action='store', 
                        help='exclude all files listed in %(dest)s')
    parser.add_argument('--language', dest='languages', nargs='+', 
                        help='list of languages to be processed (default: ' + ' '.join(languages) + ')')
    parser.add_argument('--task', dest='tasks_id', nargs='+', 
                        help='list of tasks ids or ranges (Python syntax) of task ids (default: all available tasks)')
    parser.add_argument('--list-tasks', dest='list_tasks', action='store_true', 
                        help='list available tasks and their IDs for the selected action')
    parser.add_argument('--select', dest='select_expr', nargs=1, 
                        help='filter according to a positive boolean combination of "attribute ~ value" comparisons (the default behavior is printing everything; attribute can be lang, fname, or stats[key])')
    parser.add_argument('--aggregate', dest='aggregate', action='store_true',
                        help='aggregate numeric lists by taking the mean of all elements but the first one')
    parser.add_argument('--concise', dest='concise_out', action='store_true', 
                        help='print only filenames of result')
    parser.add_argument('--short-filenames', dest='short_filenames', action='store_true', 
                        help='do not print absolute path of filenames of result')
    parser.add_argument('--serialize', dest='serialize_file', action='store', 
                        help='serialize the result to %(dest)s and deserialize if %(dest)s exists')
    parser.add_argument('--notes', dest='merge_notes', action='store', 
                        help='merge the notes in %(dest)s into the stats of the applicable tasks')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', 
                        help='print information about progress')
    parser.add_argument('action', action='store', nargs=1, choices=available_actions, 
                        help='action to be performed')
    args = parser.parse_args()
    rc_data = RosettaCodeData(data_location)
    todo = todo(todo_file, todo_keylist, 
                start_at_line=todo_start_at_line, true_key=todo_true_key)

    arg_action = args.action[0]
    if arg_action == 'loc':
        action = 'loc'
        ignore_make = False
        patch = True
    elif arg_action == 'compile':
        action = 'compile'
        ignore_make = True
        patch = False
    elif arg_action == 'make':
        action = 'compile'
        ignore_make = False
        patch = True
    elif arg_action == 'run':
        action = 'run'
        ignore_make = False
        patch = False
    elif arg_action == 'suggest-ignore' or arg_action == 'syntaxcheck':
        action = 'syntaxcheck'
        ignore_make = True
        patch = False
    available_tasks = [tsk for tsk in todo.keys() if action == 'syntaxcheck' or action in todo[tsk]]
    if args.list_tasks:
        tid = -1
        print '# task ID : task name'
        for tsk in available_tasks:
            tid += 1
            print str(tid) + ' : ' + tsk
        sys.exit(0)
    if args.languages is not None:
        languages = args.languages
    if args.tasks_id is not None:
        tsks = []
        for rng in args.tasks_id:
            try:
                el = eval('available_tasks[' + rng + ']')
            except:
                print >> sys.stderr, __file__ + ": " + "invalid task ID: " + rng
            if not isinstance(el, list):
                el = [el]
            tsks += el
        available_tasks = tsks
    if args.ignore_file is not None:
        ignore_file = args.ignore_file
        with open(ignore_file, 'r') as fignore:
            ign = [sl for sl in [line.strip() for line in fignore] if not sl.startswith('#')]
        # switch to absolute paths
        ignore = []
        for f in ign:
            rest, solution_name = os.path.split(f)
            rest, language_dirname = os.path.split(rest)
            rest, task_dirname = os.path.split(rest)
            rest, dirname = os.path.split(rest)
            ignore.append(os.path.abspath(os.path.join(
                data_location, dirname, task_dirname, language_dirname, solution_name)))
    else:
        ignore = []
    if args.merge_notes is not None:
        extra_notes = parse_notes(rc_data, args.merge_notes, script_names=language_script_names)
    else:
        extra_notes = None
    call_fun = mine
    call_args = (available_tasks, languages, language_file_extensions, rc_data, action)
    call_kwargs = {'force': True, 'ignore_make': ignore_make, 'patch': patch, 
                   'scripts_location': scripts_location, 'verbose': args.verbose, 'ignore': ignore,
                   'extra_notes': extra_notes, 'scripts_names': language_script_names }
    if args.serialize_file is not None:
        serialize_file = args.serialize_file
        result = blob(serialize_file, call_fun, *call_args, **call_kwargs)
    else:
        result = call_fun(*call_args, **call_kwargs)

    if args.concise_out:
        header = '## processed file'
    else:
        sample_task = result.keys()[0]
        sample_language = result[sample_task].keys()[0]
        sample_filename = result[sample_task][sample_language].keys()[0]
        stats_keys = sorted(result[sample_task][sample_language][sample_filename].keys())
        header = '## task : language : processed file : ' + ' : '.join(stats_keys)

    print header
    if arg_action == 'suggest-ignore':
        select = "stats['syntaxcheck'] != 0"
    elif args.select_expr is None:
        select = 'True'
    else:
        select = args.select_expr[0]
    if args.aggregate:
        afun = aggregation_function
        precision = precision_details
    else:
        afun = lambda v, task, lang, fn, key, precision : v
        precision = {}
    for task, dlang in result.iteritems():
        for lang, dfile in dlang.iteritems():
            for fname, dstats in dfile.iteritems():
                stats = dstats
                if args.short_filenames:
                    fn = os.path.basename(fname)
                else:
                    fn = fname
                if eval(select):
                    try:
                        if args.concise_out:
                            row = [dstats['target_fname']]
                        else:
                            row = [task, lang, fn] + \
                                  [str(afun(dstats[k], task, lang, fn, k, precision)) for k in stats_keys]
                    except KeyError as ke:
                        print >> sys.stderr, "error (missing key): " + str(dstats)
                        print >> sys.stderr, ke
                    print ' : '.join(row)
