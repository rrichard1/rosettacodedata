# file with todo list in CSV format
todo_file = "../data/todo.csv"
# line number where actual data starts in todo list
todo_start_at_line = 9
# list of column keys in todo list
todo_keylist = ('loc', 'compile', 'run', 'input', 'performance', 'output', 'tag')
# value corresponding to True in todo list
todo_true_key = 'x'

# (precision, n_deltas, discard if not passing?)
# round to `precision' decimal digits (do not round if precision is None)
# warning if elements differ by more than `n_deltas' standard deviations
# discarding = change into -1
precision_details = { 'user': (4, 3, False),
                      'maxram': (0, 3, False),
                      'avgram': (1, 3, False), 
                      'pfs': (1, 3, False), 
                      'timeout': (0, 0, True) }

# standard aggregation function of numeric lists
def aggregation_function (v, task="", lang="", fn="", key="", precision={}):
    import sys, numpy
    result = v
    check_std = False
    if type(v) == list:
        if len(v) == 1:
            result= v[0]
        elif len(v) > 1:
            first = v[0]
            rest = v[1:]
            if type(first) is int or type(first) is float:
                try:
                    result = sum(rest) / len(rest)
                except:
                    result = v
            elif len(set(v)) == 1:
                # all elements are the same
                result = first
            if precision.has_key(key):
                check_std = True
                mean = numpy.mean(rest)
                std = numpy.std(rest)
                if std != 0:
                    deviations = [abs(x - mean)/std for x in rest]
    if precision.has_key(key):
        n_digits = precision[key][0]
        is_int = type(result) is int
        result = round(result, n_digits)
        if is_int:
            result = int(result)
        n_deltas = precision[key][1]
        if check_std and n_deltas is not None and std != 0:
            large_devs = [d for d in deviations if d > n_deltas]
            if len(large_devs) > 0 and round(std, n_digits) != 0:
                print >> sys.stderr, "warning: " + ': '.join([task, lang, fn]) + " @ " + key + \
                    ": difference larger than " + str(n_deltas) + " standard deviations " + str(v)
                if precision[key][2]:
                    return -1
    return result

# directory of RosettaCode dump
data_location = '../../'

# list of default languages to be processed
languages = ['C'] #, 'Java', 'Python']
# extension of serialized files
pickle_ext = 'dat'
# directory of serialized files
pickle_location = '.'

# map: language --> file extension
language_file_extensions = {'C': 'c', 
                            'Python': 'py',
                            'Java': 'java',
                            'Haskell': 'hs',
                            'Go': 'go',
                            'Common Lisp': 'lisp',
                            'C sharp': 'cs',
                            'Perl 6': 'pl6',
                            'F Sharp': 'fs',
                            'Ruby': 'rb'}

# map: language --> prefix of scripts names
language_script_names = {'Common Lisp': 'Common-Lisp',
                         'C sharp': 'C-sharp',
                         'Perl 6': 'Perl-6',
                         'F Sharp': 'F-Sharp'}

# action names
available_actions = ['loc', 'compile', 'make', 'run'] # + \
                    # ['syntaxcheck',                                    'suggest-ignore']
                    # check whether files are syntactically correct,   suggest list of invalid files


# directory of scripts for compilation and other checks
scripts_location = '../comp-scripts'
