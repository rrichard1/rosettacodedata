#!/bin/bash

set -u
# set -e

LANGUAGES=(C Java Python Haskell Go 'C sharp' 'F Sharp' Ruby)
LANGUAGE_SCRIPT_NAMES=(C Java Python Haskell Go C-sharp F-Sharp Ruby)
OPERATIONS=(compile make run loc)
TASKDIR='../../Task'
CHAMPDIR=`pwd`
LOGFILE='log.log'

echo "## OPERATION LOG" > $LOGFILE

for ((nlang = 0; nlang < ${#LANGUAGES[@]}; nlang++)) do
	 lang=${LANGUAGES[$nlang]}
	 script_lang=${LANGUAGE_SCRIPT_NAMES[$nlang]}
	 for oper in ${OPERATIONS[@]}; do
		  if [ "$oper" != "run" ]; then 
		  		echo "START @ `date`: cleaning $lang" >> $LOGFILE
		  		make -C "$TASKDIR" "clean.$script_lang"
		  		echo "DONE @ `date`: cleaning $lang" >> $LOGFILE
		  fi
		  echo "START @ `date`: $oper $lang" >> $LOGFILE
		  ./champ.py --ignore "../data/ignore_$script_lang" --language "$lang" --serialize "../data/n.3mto.$oper"'_'"$script_lang.dat" --notes "../data/notes_$script_lang.txt" -v "$oper" > "$oper.$script_lang.out.txt" 2> "$oper.$script_lang.err.txt"
		  echo "DONE @ `date`: $oper $lang" >> $LOGFILE
	  done
done

# send notification email
# cat $LOGFILE | xargs ~/software/scripts/done.py 
