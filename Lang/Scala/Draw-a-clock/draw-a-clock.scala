import scala.math._
import java.util.Calendar
import java.util.Timer
import java.util.TimerTask

object Clock {
  val WIDTH: Int = 80
  val HEIGHT: Int = 35

  def makeText(grid: Array[Array[Char]], r: Double, theta: Double, str: String): Unit = {
    val (row, col) = toGridCoord(r * cos(theta), r * sin(theta))
    (0 until str.length).foreach(i => {
      if (row >= 0 && row < HEIGHT && col + i >= 0 && col + i < WIDTH) grid(row)(col + i) = str(i)
    })
  }

  def makeCircle(grid: Array[Array[Char]], r: Double, c: Char): Unit = {
    var theta = 0.0;
    while (theta < 2 * Pi) {
      val(row, col) = toGridCoord(r * cos(theta), r * sin(theta))
      if (row >= 0 && row < HEIGHT && col >= 0 && col < WIDTH) grid(row)(col) = c
      theta = theta + 0.01
    }
  }

  def makeHand(grid: Array[Array[Char]], maxR: Double, theta: Double, c: Char): Unit = {
    var r = 0.0;
    while (r < maxR) {
      val(row, col) = toGridCoord(r * cos(theta), r * sin(theta))
      if (row >= 0 && row < HEIGHT && col >= 0 && col < WIDTH) grid(row)(col) = c
      r = r + 0.01
    }
  }

  def toGridCoord(x: Double, y: Double): (Int, Int) = {
    (floor((y + 1.0) / 2.0 * HEIGHT).toInt, floor((x + 1.0) / 2.0 * WIDTH).toInt)
  }

  def printGrid(grid: Array[Array[Char]]): Unit = {
    grid.foreach(row => println(row.mkString))
  }

  def getGrid(calendar: Calendar): Array[Array[Char]] = {
    val grid = Array.tabulate(HEIGHT, WIDTH)((_,_) => ' ')
    makeCircle(grid, 0.98, '@')
    val hour = calendar.get(Calendar.HOUR);
    val minute = calendar.get(Calendar.MINUTE);
    val second = calendar.get(Calendar.SECOND);

    makeHand(grid, 0.6, (hour + minute / 60.0 + second / 3600.0) * Pi / 6 - Pi / 2, 'O')
    makeHand(grid, 0.85, (minute + second / 60.0) * Pi / 30 - Pi / 2, '*')
    makeHand(grid, 0.90, second * Pi / 30 - Pi / 2, '.')

    (1 to 12).foreach(n => {
      makeText(grid, 0.87, n * Pi / 6 - Pi / 2, n.toString)
    })

    grid
  }

  def main(args: Array[String]) = {
    val timer = new Timer
    val timerTask = new TimerTask {
      def run() = printGrid(getGrid(Calendar.getInstance))
    }
    timer.schedule(timerTask, 0, 1000)
  }
}
