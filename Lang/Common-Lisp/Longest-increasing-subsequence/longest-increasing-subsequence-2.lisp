(defun lis-patience-sort (input-list)
  (let ((piles nil))
    (dolist (item input-list)
      (setf piles (insert-item item piles)))
    (reverse (caar (last piles)))))

(defun insert-item (item piles)
  (let ((inserted nil))
    (loop for pile in piles
	  and prev = nil then (car pile)
	  and i from 0
	  do (when (and (not inserted)
			(<= item (caar pile)))
	       (setf inserted t
		     (elt piles i) (push (cons item prev) (elt piles i)))))
    (if inserted
	piles
	(append piles (list (list (cons item (caar (last piles)))))))))

(dolist (l (list (list 3 2 6 4 5 1)
		   (list 0 8 4 12 2 10 6 14 1 9 5 13 3 11 7 15)))
    (format t "~A~%" (lis-patience-sort l)))
