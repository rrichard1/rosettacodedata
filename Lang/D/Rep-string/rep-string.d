import std.stdio, std.string, std.conv, std.range, std.algorithm;

size_t repString1(in string s) pure /*nothrow*/ {
    foreach_reverse (immutable n; 1 .. s.length / 2 + 1)
        if (s.take(n).cycle.take(s.length).equal(s))
            return n;
    return 0;
}

size_t repString2(in string s) pure /*nothrow*/ {
    immutable bits = s.to!ulong(2);

    foreach_reverse (immutable left; 1 .. s.length / 2 + 1) {
        immutable right = s.length - left;
        if ((bits ^ (bits >> left)) == ((bits >> right) << right))
            return left;
    }

    return 0;
}

void main() {
    immutable words = "1001110011 1110111011 0010010010 1010101010
                       1111111111 0100101101 0100100 101 11 00 1";

    foreach (immutable w; words.split) {
        immutable r1 = w.repString1;
        assert(r1 == w.repString2);
        if (r1)
            w.chunks(r1).join(" ").writeln;
        else
            writeln(w, " (no repeat)");
    }
}
