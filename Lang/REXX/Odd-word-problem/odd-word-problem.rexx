/*REXX program solves the  odd word  problem  by just using  byte I/O.  */
iFID_ = 'ODDWORD.IN'                   /*numeric suffix is added later. */
oFID_ = 'ODDWORD.'                     /*   "       "    "   "     "    */
      do n=1  for 2;  iFID=ifid_ || n;   #=0   /*#=number of chars read.*/
                      oFID=ofid_ || n
      say;  say '──────── reading file:' iFID "────────";  #=0
               do  until x=='.'        /* [↓]    perform for odd words. */
                  do until \datatype(x,'M'); call readChar; call writeChar
                  end   /*until \datatype···*/
               if x=='.'  then leave
                                       /* [↓]    perform for even words.*/
               call readLetters;   punct#=#
                  do j=punct#-1  by -1;                    call readChar j
                  if \datatype(x,'M')  then leave;         call writeChar
                  end   /*j*/
               call readLetters;       call writeChar;     #=punct#
               end   /*until x=='.' */
      end            /*n*/             /* [↑]  process both input files.*/
exit                                   /*stick a fork in it, we're done.*/
/*──────────────────────────────────readChar subroutine─────────────────*/
readChar: if lines(iFID)==0 then say '***error!*** EOF reached.'
if arg(1,'O')  then do; x=charin(ifid); #=#+1; end  /*read the next char*/
               else x=charin(ifid,arg(1))           /*read specific char*/
return
/*──────────────────────────────────readLetters subroutine──────────────*/
readLetters: do  until  \datatype(x,'M');   call readChar;  end;    return
/*──────────────────────────────────writeChar subroutine────────────────*/
writeChar:   call charout ,x;   call charout oFID,x;   return
