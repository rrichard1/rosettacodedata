/*REXX pgm create an indexable string lower|uppercase ASCII chars a──►z */
$L=''                                  /*set lowercase  letters to null.*/
$U=''                                  /* "  uppercase     "     "   "  */
    do j=0  for 2**8                   /*traipse through all the chars. */
    _=d2c(j)                           /*convert decimal number to char.*/
    if datatype(_,'L')  then $L=$L||_  /*Lowercase?   Then add to list. */
    if datatype(_,'U')  then $U=$U||_  /*Uppercase?     "   "   "   "   */
    end   /*j*/                        /* [↑] put all letters into lists*/
                                       /*stick a fork in it, we're done.*/
