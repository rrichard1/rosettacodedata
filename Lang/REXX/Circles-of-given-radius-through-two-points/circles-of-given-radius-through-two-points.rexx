/*REXX pgm finds 2 circles with a specific radius given two (X,Y) points*/
@. =
@.1=  0.1234    0.9876    0.8765     0.2345     2
@.2=  0.0000    2.0000    0.0000     0.0000     1
@.3=  0.1234    0.9876    0.1234     0.9876     2
@.4=  0.1234    0.9876    0.8765     0.2345     0.5
@.5=  0.1234    0.9876    0.1234     0.9876     0
say '     x1        y1        x2        y2     radius            cir1x     cir1y     cir2x     cir2y'
say '  ────────  ────────  ────────  ────────  ──────          ────────  ────────  ────────  ────────'
      do  j=1  while  @.j\==''         /*process all given points&radius*/
         do k=1  for 4;   w.k=f(word(@.j,k));  end /*k*/   /*format num.*/
      say w.1 w.2 w.3 w.4 center(word(@.j,5),9)  "───► "  twoCircles(@.j)
      end           /*j*/
exit                                   /*stick a fork in it, we're done.*/
/*──────────────────────────────────F subroutine────────────────────────*/
f: return right(format(arg(1),,4),9)   /*format a number with 4 dec dig.*/
/*──────────────────────────────────SQRT subroutine─────────────────────*/
sqrt: procedure; parse arg x; if x=0 then return 0; d=digits();numeric digits 11
      g=.sqrtGuess();       do j=0 while p>9;  m.j=p;  p=p%2+1;   end
      do k=j+5 to 0 by -1; if m.k>11 then numeric digits m.k; g=.5*(g+x/g); end
      numeric digits d;  return g/1
.sqrtGuess: if x<0 then call sqrtErr;   numeric form;   m.=11;   p=d+d%4+2
      parse value format(x,2,1,,0) 'E0' with g 'E' _ .;   return g*.5'E'_%2
/*──────────────────────────────────twoCircles subroutine───────────────*/
twoCircles:  procedure;   parse arg px py qx qy r .
if r=0        then return 'radius of zero gives no circles.'
x=(qx-px)/2;   y=(qy-py)/2;    bx=px+x;    by=py+y;     pb=sqrt(x**2+y**2)
if pb=0       then return 'coincident points give infinite circles'
if pb>r       then return 'points are too far apart for the given radius'
cb=sqrt(r**2-pb**2);      x1=y*cb/pb;     y1=x*cb/pb
return   f(bx-x1)   f(by+y1)   f(bx+x1)   f(by-y1)
