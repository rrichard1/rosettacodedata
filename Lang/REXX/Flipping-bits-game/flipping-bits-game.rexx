/*REXX program presents a "flipping bit" puzzle to the user at the C.L. */
parse arg N u on off .;       tries=0  /*get optional arguments.        */
if N=='' | N==','  then N=3            /*Size given?   Then use default.*/
if u=='' | u==','  then u=3            /*number of bits initialized  ON.*/
if on==''          then on=1           /*character used for  "on".      */
if off==''         then off=0          /*character used for  "off".     */
@.=off                                 /*set the array to  "off"  chars.*/
       do while it()<u                 /* [↓]   turn "on"  U   elements.*/
       r=random(1,N); c=random(1,N); @.r.c=on   /*set  row,column  to ON*/
       end   /*while*/                 /* [↑] keep going 'til U bits set*/
oz=z                                   /*remember the original array str*/
call it 20, '   ◄───target'            /*show target for user to attain.*/
do random(1,2); call flip 'R',random(1,N);  call flip 'C',random(1,N); end
if z==oz   then call flip 'R',random(1,N) /*ensure it's not the original*/
       do until z==oz                  /*prompt until they get it right.*/
       if tries\==0  then  say '─────────bit array after move: '    tries
       call it 1                       /*display the array to the screen*/
       call prompt                     /*get a row or column # from C.L.*/
       call flip left(?,1),substr(?,2) /*flip a user selected row or col*/
       call it                         /*get image of the updated array.*/
       end   /*forever*/
say; say '─────────Congrats!    You did it in'     tries     "tries."
exit tries                             /*stick a fork in it, we're done.*/
/*──────────────────────────────────FLIP subroutine─────────────────────*/
flip: parse arg x,#                    /*x is  R  or  C,  # is which one*/
if x=='R' then do c=1 for N; if @.#.c==on then @.#.c=off;else @.#.c=on;end
          else do r=1 for N; if @.r.#==on then @.r.#=off;else @.r.#=on;end
return
/*──────────────────────────────────IT subroutine───────────────────────*/
it: z=; $=0; _=;  parse arg tell,tx;   if tell\==''  then say
         do r=1  for N
           do c=1  for N;  z=z||@.r.c;   _=_ @.r.c;   $=$+(@.r.c==on); end
         if tell\==''  then say left('',tell) _ tx; _= /*show array?*/
         end     /*r*/
return $
/*──────────────────────────────────PROMPTER subroutine─────────────────*/
prompt: p='─────────Please enter a row or col number (as  r1  or  c3),   or  Quit:'
   do forever;   ok=1;   say;   say p;  parse upper pull ?;   ?=space(?,0)
   parse var ? what 2 num;              if abbrev('QUIT',?,1)  then exit 0
   if what\=='R' & what\=='C'  then call terr 'first char not  R  or  C'
   if \datatype(num,W)         then call terr 'row  or  col  not numeric'
                               else num=num/1          /*normalize the #*/
   if num<1 | num>N            then call terr 'row  or  col  out of range'
   if \ok  then iterate                                /*had any errors?*/
   tries=tries+1;   return ?                           /*bump counter.  */
   end   /*forever*/
/*──────────────────────────────────TERR subroutine─────────────────────*/
terr: if ok  then say  '***error!***:'  arg(1);        ok=0;        return
