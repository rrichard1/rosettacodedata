left_fact = Enumerator.new do |y|
  n, f, lf = 0, 1, 0
  loop do
    y  << lf #yield left_factorial
    n  += 1
    lf += f
    f  *= n
  end
end

tens = 20.step(110, 10).to_a
thousands = 1000.step(10_000, 1000).to_a

10001.times do |n|
  lf = left_fact.next
  case n
  when 0..10, *tens
    puts "!#{n} = #{lf}"
  when *thousands
    puts "!#{n} has #{lf.to_s.size} digits"
  end
end
