def cut_it(h, w)
  dirs = [[1, 0], [-1, 0], [0, -1], [0, 1]]
  if h.odd?
    h, w = w, h
    return 0  if h.odd?
  end
  return 1  if w == 1

  nxt = [w+1, -w-1, -1, 1]
  blen = (h + 1) * (w + 1) - 1
  grid = [false] * (blen + 1)

  walk = lambda do |y, x, count|
    return count+1  if y==0 or y == h or x==0 or x == w
    t = y * (w + 1) + x
    grid[t] = grid[blen - t] = true
    4.times do |i|
      unless grid[t + nxt[i]]
        count = walk[y + dirs[i][0], x + dirs[i][1], count]
      end
    end
    grid[t] = grid[blen - t] = false
    count
  end

  t = h / 2 * (w + 1) + w / 2
  if w.odd?
    grid[t] = grid[t + 1] = true
    res   = walk[h / 2, w / 2 - 1, 0]
    count = walk[h / 2 - 1, w / 2, 0]
    res + count * 2
  else
    grid[t] = true
    count = walk[h / 2, w / 2 - 1, 0]
    return count * 2  if h == w
    walk[h / 2 - 1, w / 2, count]
  end
end

for w in 1...10
  for h in 1..w
    puts "%d x %d: %d" % [w, h, cut_it(w, h)]  if (w * h).even?
  end
end
