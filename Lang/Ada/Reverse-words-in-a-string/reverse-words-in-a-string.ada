with Ada.Text_IO;

procedure Reverse_Words is

   function Reverse_Words(S: String) return String is

      function First_Word(S: String) return String is
	 -- this is the "parser", to recognise the first word in every string
	 Start: Positive := S'First;
	 Stop: Natural;
      begin
	 while Start <= S'Last and then S(Start) = ' ' loop
	    Start := Start + 1;
	 end loop; -- now S(Start) is the first non-space,
                   -- or Start = S'Last+1 if S is enpty or space-only
	 Stop := Start-1; -- now S(Start .. Stop) = ""
	 while Stop < S'Last and then S(Stop+1) /= ' ' loop
	    Stop := Stop + 1;
	 end loop; -- now S(Stop+1) is the first sopace after Start
	           -- or Stop = S'Last if there is no such space
	 return S(Start .. Stop);
      end First_Word;

      Head: String := First_Word(S);
      Tail: string := S(S'First + Head'Length + 1 .. S'Last);
   begin
      if Head = "" then
	 return "";
      else
	 return Reverse_Words(Tail) & " " & Head;
      end if;
   end Reverse_Words;

   use Ada.Text_IO;
begin
   while not End_Of_File loop
      Put_Line(Reverse_Words(Get_Line)); -- poem is read from standard input
   end loop;
end Reverse_Words;
