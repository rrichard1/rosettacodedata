#!/usr/bin/awk -f
BEGIN { FS=",";}
{
	if (NR==1) {
		print $0""FS"SUM";
	}
	else {
		s = 0;
		for (k=1; k<=NF; k++) {	
			s += $k;
		}
		print $0""FS""s;
	}
}
