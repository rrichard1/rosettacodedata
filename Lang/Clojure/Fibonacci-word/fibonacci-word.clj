(defn entropy [s]
  (let [len (count s)
        freqs (frequencies s)
        log-of-2 (Math/log 2)]
    (->> (keys freqs)
         (map (fn [c]
                (let [rf (/ (get freqs c) len)]
                  (Math/abs (* rf (/ (Math/log rf) log-of-2))))))
         (reduce +))))

(defn fibw [n]
  (loop [n n a \1 b \0]
    (if (== 1 n)
      (str a)
      (recur (dec n) b (str b a)))))

(do
  (printf "%2s %10s %17s %s%n" "N" "Length" "Entropy" "Fibword")
  (doseq [i (range 1 38)]
    (let [w (fibw i)]
      (printf "%2d %10d %.15f %s%n" i (count w) (entropy w) (if (<= i 8) w "...")))))
