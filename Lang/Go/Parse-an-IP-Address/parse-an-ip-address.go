package main

import (
    "fmt"
    "net"
    "strconv"
)

var testCases = []string{
    "127.0.0.1",
    "127.0.0.1:80",
    "::1",
    "[::1]:80",
    "2605:2700:0:3::4713:93e3",
    "[2605:2700:0:3::4713:93e3]:80",
}

func main() {
    var max int
    for _, addr := range testCases {
        if len(addr) > max {
            max = len(addr)
        }
    }
    fmt.Printf("%-*s  ", max, "Input")
    fmt.Println("                         Address  Space  Port")
    for _, addr := range testCases {
        fmt.Printf("%-*s  ", max, addr)
        ip := net.ParseIP(addr)
        var host, port string
        var err error
        if ip == nil {
            host, port, err = net.SplitHostPort(addr)
            if err != nil {
                fmt.Println(err)
                continue
            }
            ip = net.ParseIP(host)
        }
        if ip == nil {
            fmt.Println("Invalid address format")
            continue
        }
        if port > "" {
            pn, err := strconv.Atoi(port)
            if err != nil {
                fmt.Println("Invalid port: ", err)
                continue
            }
            if pn < 0 || pn > 65535 {
                fmt.Println("Invalid port number")
                continue
            }
        }
        space := "ipv6"
        if ip4 := ip.To4(); ip4 != nil {
            space = "ipv4"
            fmt.Print("                        ")
            ip = ip4
        }
        for _, b := range ip {
            fmt.Printf("%02x", b)
        }
        fmt.Printf("  %s   %s\n", space, port)
    }
}
