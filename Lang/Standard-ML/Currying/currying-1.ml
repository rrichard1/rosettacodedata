fun addnums x:int y = x+y    (* declare a curryable function *)

val add1 = addnums 1         (* bind the first argument to get another function *)
addnums 42                   (* apply to actually compute a result, 43 *)
