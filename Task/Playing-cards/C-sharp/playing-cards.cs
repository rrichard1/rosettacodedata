using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

class Card {
    string pip;
    string suit;

    public static string[] pips = { "Two","Three","Four","Five","Six","Seven","Eight",
        "Nine","Ten","Jack","Queen","King","Ace"};

    public static string[] suits = { "Diamonds", "Spades", "Hearts", "Clubs" };

    public Card(string pip, string suit) {
        this.pip = pip;
        this.suit = suit;
    }

    public override string ToString() {
        return String.Format("{0} of {1}", pip, suit);
    }
}

class Deck {
    public List<Card> deck = new List<Card>();

    public Deck() {
        foreach (string pip in Card.pips) {
            foreach (string suits in Card.suits) {
                deck.Add(new Card(pip, suits));
            }
        }
    }

    public void Shuffle() {
        // using Knuth Shuffle (see at http://rosettacode.org/wiki/Knuth_shuffle)
        Random random = new System.Random();
        Card temp;
        int j;

        for (int i = 0; i < deck.Count; i++){
            j = random.Next(deck.Count);
            temp = deck[i];
            deck[i] = deck[j];
            deck[j] = temp;
        }
    }

    public Card Deal() {
        Card r = this.deck[0];
        this.deck.RemoveAt(0);
        return r;
    }

    public override string ToString() {
        return String.Join(Environment.NewLine, this.deck.Select(x => x.ToString()).ToArray());
    }
}
