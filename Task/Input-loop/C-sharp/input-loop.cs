using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        StreamReader b = new StreamReader("file.txt"); //or any other Stream

        string line = b.ReadLine();

        while (line != null)
        {
            Console.WriteLine(line);
            line = b.ReadLine();
        }
    }
}
