sub fft {
    return @_ if @_ == 1;
    my @evn = fft( @_[0, 2 ... *] );
    my @odd = fft( @_[1, 3 ... *] );
    @odd »*=» (1, * * exp( 2i * pi / @_ ) ... *);
    return @evn »+« @odd, @evn »-« @odd;
}

my @seq    = ^16;
my $cycles = 3;
my @wave   = map { sin( 2*pi * $_ / @seq * $cycles ) }, @seq;
say "wave: ", @wave.fmt("%7.3f");

say "fft:  ", fft(@wave)».abs.fmt("%7.3f");
