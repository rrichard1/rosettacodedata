let memoize f =
    let cache = System.Collections.Generic.Dictionary<_,_>()
    fun x ->
        match cache.TryGetValue(x) with
        | (true, v) -> v
        | (_, _) ->
            let v = f x
            cache.[x] <- v
            v

let rec q = memoize (fun i ->
    if i < 3I then 1I
    else q (i - q (i - 1I)) + q (i - q(i - 2I)))

printf "q(1 .. 10) ="; List.iter (q >> (printf " %A")) [1I .. 10I]
printfn ""
printfn "q(1000) = %A" (q 1000I)
printfn "descents(100000) = %A" (Seq.sum  (Seq.init 100000 (fun i -> if q(bigint(i)) > q(bigint(i+1)) then 1 else 0)))
