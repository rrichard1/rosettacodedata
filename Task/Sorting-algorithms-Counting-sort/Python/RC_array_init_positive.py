def RC_array_init(array, num, seed):
      LCG_A = 1664525
      LCG_C = 1013904223
      for j in range(num):
        seed = (LCG_A * seed + LCG_C) & 0x7fffffff
        array.append(seed % num);
      return array
