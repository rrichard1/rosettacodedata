let rand = System.Random()

while true do
  let rec randomNums() =
    let xs = [|for i in 1..9 -> rand.Next(), i|] |> Array.sort |> Array.map snd
    if xs = Array.sort xs then randomNums() else xs

  let xs = randomNums()

  while xs <> Array.sort xs do
    printf "\n%A\n\nReverse how many digits?\n> " xs
    let n = stdin.ReadLine() |> int
    Array.blit (Array.rev xs.[0..n-1]) 0 xs 0 n
  printf "Well done!\n\n"
