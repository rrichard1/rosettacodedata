let isPrime x =
  if x < 2 then false else
  if x = 2 then true else
  if x % 2 = 0 then false else
  let sr = int (sqrt (double x))
  let rec test n =
    if n > sr then true else
    if x % n = 0 then false else test (n + 2) in test 3
