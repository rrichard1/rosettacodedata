public class Matrix
{
	int n;
	int m;
	double[,] a;

	public Matrix(int n, int m)
	{
		if (n <= 0 || m <= 0)
			throw new ArgumentException("Matrix dimensions must be positive");
		this.n = n;
		this.m = m;
		a = new double[n, m];
	}

	//indices start from one
	public double this[int i, int j]
	{
		get { return a[i - 1, j - 1]; }
		set { a[i - 1, j - 1] = value; }
	}

	public int N { get { return n; } }
	public int M { get { return m; } }

	public static Matrix operator*(Matrix _a, Matrix b)
	{
		int n = _a.N;
		int m = b.M;
		int l = _a.M;
		if (l != b.N)
			throw new ArgumentException("Illegal matrix dimensions for multiplication. _a.M must be equal b.N");
		Matrix result = new Matrix(_a.N, b.M);
		for(int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
			{
				double sum = 0.0;
				for (int k = 0; k < l; k++)
					sum += _a.a[i, k]*b.a[k, j];
				result.a[i, j] = sum;
			}
		return result;
	}
}
