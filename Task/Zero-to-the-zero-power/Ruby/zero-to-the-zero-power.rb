require 'complex'
require 'rational'
require 'bigdecimal'

[0, 0.0, Complex(0), Rational(0), BigDecimal.new("0")].each {|n|
  printf "%10s: ** -> %s\n" % [n.class, n**n]
}
