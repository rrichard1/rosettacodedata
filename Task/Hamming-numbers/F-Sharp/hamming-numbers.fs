type LazyList<'a> = Cons of 'a * Lazy<LazyList<'a>>

let rec inf_map f = function
    | Cons(x, g) -> Cons(f x, lazy(inf_map f (g.Force())))

let rec (-|-) (Cons(x, f) as xs) (Cons(y, g) as ys) =
    if x < y then Cons(x, lazy(f.Force() -|- ys))
    elif x > y then Cons(y, lazy(xs -|- g.Force()))
    else Cons(x, lazy(f.Force() -|- g.Force()))

let rec hamming =
    Cons(1, lazy(let x = inf_map ((*) 2) hamming
                 let y = inf_map ((*) 3) hamming
                 let z = inf_map ((*) 5) hamming
                 x -|- y -|- z))

[<EntryPoint>]
let main args =
    let a = ref hamming
    let i = ref 0
    while !i < 100 do
        match !a with
        | Cons (x, f) ->
            printf "%d, " x
            a := f.Force()
            i := !i + 1
    0
