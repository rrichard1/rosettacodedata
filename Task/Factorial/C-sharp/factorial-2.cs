using System;

class Program
{
    static int Factorial(int number)
    {
        return number == 0 ? 1 : number * Factorial(number - 1);
    }

    static void Main()
    {
        Console.WriteLine(Factorial(10));
    }
}
