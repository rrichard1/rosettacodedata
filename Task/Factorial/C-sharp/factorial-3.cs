using System;

class Program
{
    static int Factorial(int number)
    {
        return Factorial(number, 1);
    }

    static int Factorial(int number, int accumulator)
    {
        return number == 0 ? accumulator : Factorial(number - 1, number * accumulator);
    }

    static void Main()
    {
        Console.WriteLine(Factorial(10));
    }
}
