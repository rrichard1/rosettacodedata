private static double Phi = ((1d + Math.Sqrt(5d))/2d);
private static double D = 1d/Math.Sqrt(5d);

ulong Fib(uint n) {
    if(n > 92) throw new ArgumentOutOfRangeException("n", n, "Needs to be smaller than 93.");
    return (ulong)((Phi^n) - (1d - Phi)^n))*D);
}
