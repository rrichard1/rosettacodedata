// Usage: Singleton.Instance.SomeMethod()
class Singleton
{
        private static Singleton _singleton;
        private static object lockObject = new object();
        private Singleton() {}
        public static Singleton Instance
        {
            get
            {
                lock(lockObject)
                {
                    if (_singleton == null)
                        _singleton = new Singleton();
                }
                return _singleton;
            }
        }
        // The rest of the methods
}
