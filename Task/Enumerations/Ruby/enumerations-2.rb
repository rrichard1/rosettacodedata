module Card
  # constants
  SUITS = %w(Clubs Hearts Spades Diamonds)
  SUIT_VALUE = Hash[ SUITS.each_with_index.to_a ]
  #=> {"Clubs"=>0, "Hearts"=>1, "Spades"=>2, "Diamonds"=>3}

  PIPS = %w(2 3 4 5 6 7 8 9 10 Jack Queen King Ace)
  PIP_VALUE = Hash[ PIPS.each.with_index(2).to_a ]
  #=> {"2"=>2, "3"=>3, "4"=>4, "5"=>5, "6"=>6, "7"=>7, "8"=>8, "9"=>9, "10"=>10, "Jack"=>11, "Queen"=>12, "King"=>13, "Ace"=>14}
end
