(let Str "Rosetta code"
   (pack
      (mapcar '((B) (pad 2 (hex B)))
         (native "libcrypto.so" "SHA256" '(B . 32) Str (length Str) '(NIL (32))) ) ) )
