using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Parallel;
namespace Test {
    class Program {
        static void Main(string[] args) {
            /*
             * We Use Linq To Determine The Median
             * It could be done ofcourse the Normal way
             */
            List<double> myList = new List<double>() { 1, 5, 3, 6, 4, 2 };

            var query =     from numbers in myList //select the numbers
                            orderby numbers ascending
                            select numbers;

            if (myList.Count % 2 == 0) { //we know its even
                int element = myList.Count / 2; ;
                double median = (double)((query.ElementAt(element - 1) + query.ElementAt(element))/2);
                Console.WriteLine(median);
            } else {
               //we know its odd
                double element = (double)myList.Count / 2;
                element = Math.Round(element, MidpointRounding.AwayFromZero);
                double median = (double)query.ElementAt((int)(element - 1));
                Console.WriteLine(median);
            }
            Console.ReadLine();
        }
    }
}
