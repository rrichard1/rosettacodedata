/*REXX pgm generates a random starting position for the  Chess960  game.*/
parse arg seed .                       /*allow for (RAND) repeatability.*/
if seed\==''  then call random ,,seed  /*if SEED specified, use the seed*/
@.=.                                   /*define the (empty) first rank. */
r1=random(1,6);       @.r1='R'         /*place the  first rook on rank1.*/
rm=r1-1;              rp=r1+1          /*used for faster comparisons.   */
  do forever;  r2=random(1,8)          /*try to get a random second rook*/
  if r2==r1 | r2==rm | r2==rp  then iterate /*position of 2nd rook ¬good*/
  leave                                /*found a good 2nd rook placement*/
  end   /*forever*/                    /* [↑]  separate IFs for speed.  */
@.r2='r'                               /*place the second rook on rank1.*/
c1=min(r1,r2);  c2=max(r1,r2)          /*order the two rooks for RANDOM.*/
_ =random(c1+1,c2-1);    @._ ='K'      /*  "    "   only  king  "   "   */
          do _=0      ; b1=random(1,8);  if @.b1\==. then iterate; c=b1//2
            do forever; b2=random(1,8) /* c=color of bishop ───────┘    */
            if @.b2\==. | b2==b1 | b2//2==c    then iterate;       leave _
            end   /*forever*/          /* [↑]  find a place: 1st bishop.*/
          end     /*_*/                /* [↑]    "  "   "    2nd    "   */
@.b1='B'                               /*place the  1st  bishop on rank1*/
@.b2='b'                               /*  "    "   2nd     "    "   "  */
                                       /*place two knights on rank1.    */
   do  until @._='N'; _=random(1,8); if @._\==. then iterate; @._='N'; end
   do  until @.!='n'; !=random(1,8); if @.!\==. then iterate; @.!='n'; end
_=                                     /*only the queen is left to place*/
   do i=1  for 8;  _=_ || @.i;   end   /*construct output:  first rank. */
say translate(translate(_, 'q', .))    /*stick a fork in it, we're done.*/
