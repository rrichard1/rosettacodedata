class
    APPLICATION
create
    make

feature
    make
            -- Create and print sorted set
		do
 			create a.make(1, 30000)
         create isort.make(a)
 		   RC_array_init (a, 30000, 100)
         isort.sort
        end

     a: ARRAY [INTEGER_32]
     isort: INSERTION_SORT
 
 	 RC_array_init (a_array: ARRAY [INTEGER_32]; num: INTEGER; seed: INTEGER_32)
 	   local
 			lcg_a: INTEGER_32
 			lcg_c: INTEGER_32
 			l_seed: INTEGER_32
 		do
 			l_seed := seed
 			lcg_a := 1664525
 			lcg_c := 1013904223
 			across 1 |..| num as j loop
 				l_seed := (lcg_a * l_seed + lcg_c) & 0x7fffffff
 				a_array [j.item] := l_seed
 		   end
 		end
		  
end
