class
    INSERTION_SORT

create
    make

feature
	a: ARRAY [INTEGER_32]

 	make (a_array: ARRAY [INTEGER_32])
 		do
 			a := a_array
 		end
	
    sort
            -- Insertion sort
        local
            l_j: INTEGER
            l_value: INTEGER_32
        do
            across 2 |..| a.count as ii loop
                from
                    l_j := ii.item - 1
                    l_value := a.at (ii.item)
                until
                    l_j < 1 or a.at (l_j) <= l_value
                loop
                    a.at (l_j + 1) := a.at (l_j)
                    l_j := l_j - 1
                end
                a.at (l_j + 1) := l_value
            end
        end

end
