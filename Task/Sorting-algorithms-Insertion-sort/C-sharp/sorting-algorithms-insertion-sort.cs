using System;

namespace insertionsort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] A = new int[] { 3, 9, 4, 6, 8, 1, 7, 2, 5 };
            insertionSort(ref A);
            Console.WriteLine(string.Join(",", A));
        }

        public static void insertionSort (ref int[] A){
            for (int i = 0; i < A.Length; i++)
            {
                int value = A[i], j = i-1;
                while (j >= 0 && A[j] > value)
                {
                    A[j + 1] = A[j];
                    j--;
                }
                A[j + 1] = value;
            }
        }
    }
}
