using System;

class Program
{
    delegate Func<int, int> Recursive(Recursive recursive);

    static void Main()
    {
        Func<Func<Func<int, int>, Func<int, int>>, Func<int, int>> Y =
            f => ((Recursive)(g => (f(x => g(g)(x)))))((Recursive)(g => f(x => g(g)(x))));

        var fac = Y(f => x => x < 2 ? 1 : x * f(x - 1));
        var fib = Y(f => x => x < 2 ? x : f(x - 1) + f(x - 2));

        Console.WriteLine(fac(6));
        Console.WriteLine(fib(6));
    }
}
