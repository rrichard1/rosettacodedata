type 'a mu = Roll of ('a mu -> 'a)  // ease syntax colouring confusion with '

let unroll (Roll x) = x
//val unroll : 'a mu -> 'a

let fix f = (fun x a -> f (unroll x x) a) (Roll (fun x a -> f (unroll x x) a))
//val fix : (('a -> 'b) -> 'a -> 'b) -> 'a -> 'b = <fun>

let fac f = function
    0 -> 1
  | n -> n * f (n-1)
//val fac : (int -> int) -> int -> int = <fun>

let fib f = function
    0 -> 0
  | 1 -> 1
  | n -> f (n-1) + f (n-2)
//val fib : (int -> int) -> int -> int = <fun>

fix fac 5;;
// val it : int = 120

fix fib 8;;
// val it : int = 21
