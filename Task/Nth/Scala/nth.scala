object Nth extends App {
  def ordinalAbbrev(n: Int) = {
    val ans = "th" //most of the time it should be "th"
    if (n % 100 == 10 ) ans //teens are all "th"
    else (n % 10) match {
      case 1 => "st"
      case 2 => "nd"
      case 3 => "rd"
      case _ => ans
    }
  }

  def abbrevNumber(i: Int) { print(i + ordinalAbbrev(i) + " ") }

  (0 to 25).foreach(abbrevNumber)
  println()
  (250 to 265).foreach(abbrevNumber)
  println();
  (1000 to 1025).foreach(abbrevNumber)
}
