import java.util.Locale
import com.sun.speech.freetts._

import javax.speech.Central
import javax.speech.synthesis.{ Synthesizer, SynthesizerModeDesc }

object ScalaSpeaker extends App {

  def speech(text: String) = {
    if (!text.trim.isEmpty()) {
      val VOICENAME = "kevin16"

     System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory")
      Central.registerEngineCentral("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral")

      val synth = Central.createSynthesizer(new SynthesizerModeDesc(Locale.US))
      synth.allocate()

      val desc = synth.getEngineModeDesc() match {
        case g2: SynthesizerModeDesc => g2
      }

      synth.getSynthesizerProperties()
        .setVoice(desc.getVoices().find(_.toString() == VOICENAME).get)
      synth.speak(text, null)

      synth.waitEngineState(Synthesizer.QUEUE_EMPTY)
      synth.deallocate()
    }
  }

  speech("If it ain't Dutch, It ain't much.")
}
