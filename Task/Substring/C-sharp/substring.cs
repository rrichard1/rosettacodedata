using System;
namespace SubString
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "0123456789";
            const int n = 3;
            const int m = 2;
            const char c = '3';
            const string z = "345";

            Console.WriteLine(s.Substring(n, m));
            Console.WriteLine(s.Substring(n, s.Length - n));
            Console.WriteLine(s.Substring(0, s.Length - 1));
            Console.WriteLine(s.Substring(s.IndexOf(c,0,s.Length), m));
            Console.WriteLine(s.Substring(s.IndexOf(z, 0, s.Length), m));
        }
    }
}
