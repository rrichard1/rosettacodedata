open System

let seasons = [| "Chaos"; "Discord"; "Confusion"; "Bureaucracy"; "The Aftermath" |]

let ddate (date:DateTime) =
    let dyear = date.Year + 1166
    let leapYear = DateTime.IsLeapYear(date.Year)

    if leapYear && date.Month = 2 && date.Day = 29 then
        sprintf "St. Tib's Day, %i YOLD" dyear
    else
        // compensate for St. Tib's Day
        let dayOfYear = if leapYear && date.DayOfYear >= 60 then date.DayOfYear - 1 else date.DayOfYear
        let season, dday = Math.DivRem(dayOfYear, 73)
        sprintf "%s %i, %i YOLD" seasons.[season] dday dyear
