using System.Net.Sockets;

class Program
{
	static void Main(string[] args)
	{
		TcpListener server = new TcpListener(8000);
		server.Start();

		Console.WriteLine("Listening, port 8000");

		TcpClient client;
		do
		{
			// Accept client
			client = server.AcceptTcpClient();
			Console.WriteLine("Recieved client: " + client.Client.AddressFamily.ToString());

			// Recieve
			string tRecieve = "";
			char t;
			do
			{
				if (client.Available > 0)
				{
					t = (char)client.GetStream().ReadByte();

					if (t == 0)
						break;

					tRecieve += t;
				}
			} while (true);

			Console.WriteLine("Recieved: " + tRecieve);

			// Send
			byte[] tSend = Encoding.ASCII.GetBytes("Hello World!");
			client.GetStream().Write(tSend, 0, tSend.Length);
			client.GetStream().WriteByte(0);

			Console.WriteLine("Sent: " + Encoding.ASCII.GetString(tSend));

			// Close
			client.Close();
		} while (true);

	}
}
