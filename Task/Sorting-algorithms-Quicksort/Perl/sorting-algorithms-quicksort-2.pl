sub quicksort
{
    my $aref = shift;
    return undef if (ref $aref ne ref []);
    return @$aref if @$aref < 2;
    my $pivot = splice(@$aref, int rand @$aref, 1);

    quicksort( [ grep { $_ <  $pivot } @$aref ] ), $pivot,
    quicksort( [ grep { $_ >= $pivot } @$aref ] );
}
