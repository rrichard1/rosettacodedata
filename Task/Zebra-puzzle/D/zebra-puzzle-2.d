import std.stdio, std.math, std.traits, std.typetuple, std.typecons,
       permutations1;

enum Number : uint { One,      Two,     Three,  Four,       Five   }
enum Color  : uint { Red,      Green,   Blue,   White,      Yellow }
enum Drink  : uint { Milk,     Coffee,  Water,  Beer,       Tea    }
enum Smoke  : uint { PallMall, Dunhill, Blend,  BlueMaster, Prince }
enum Pet    : uint { Dog,      Cat,     Zebra,  Horse,      Bird   }
enum Nation : uint { British,  Swedish, Danish, Norvegian,  German }

auto nullableRef(T)(ref T item) pure nothrow @nogc {
    return NullableRef!T(&item);
}

bool isPossible(NullableRef!(immutable Number[5]) number,
                NullableRef!(immutable Color[5]) color=null,
                NullableRef!(immutable Drink[5]) drink=null,
                NullableRef!(immutable Smoke[5]) smoke=null,
                NullableRef!(immutable Pet[5]) pet=null)
pure nothrow @safe @nogc {
  if ((!number.isNull && number[Nation.Norvegian] != Number.One) ||
      (!color.isNull && color[Nation.British] != Color.Red) ||
      (!drink.isNull && drink[Nation.Danish] != Drink.Tea) ||
      (!smoke.isNull && smoke[Nation.German] != Smoke.Prince) ||
      (!pet.isNull && pet[Nation.Swedish] != Pet.Dog))
    return false;

  if (number.isNull || color.isNull || drink.isNull || smoke.isNull ||
      pet.isNull)
    return true;

  foreach (immutable i; 0 .. 5) {
    if ((color[i] == Color.Green && drink[i] != Drink.Coffee) ||
        (smoke[i] == Smoke.PallMall && pet[i] != Pet.Bird) ||
        (color[i] == Color.Yellow && smoke[i] != Smoke.Dunhill) ||
        (number[i] == Number.Three && drink[i] != Drink.Milk) ||
        (smoke[i] == Smoke.BlueMaster && drink[i] != Drink.Beer)||
        (color[i] == Color.Blue && number[i] != Number.Two))
      return false;

    foreach (immutable j; 0 .. 5) {
      if (color[i] == Color.Green && color[j] == Color.White &&
          number[j] - number[i] != 1)
        return false;

      immutable int diff = abs(number[i] - number[j]);
      if ((smoke[i] == Smoke.Blend &&
           pet[j] == Pet.Cat && diff != 1) ||
          (pet[i] == Pet.Horse &&
           smoke[j] == Smoke.Dunhill && diff != 1) ||
          (smoke[i] == Smoke.Blend &&
           drink[j] == Drink.Water && diff != 1))
        return false;
    }
  }

  return true;
}

alias N = nullableRef;

void main() {
  static immutable int[5][] perms = [0, 1, 2, 3, 4].permutations;
  immutable nation = [EnumMembers!Nation];

  // Not nice casts:
  static immutable permsNumber = cast(immutable Number[5][])perms,
                   permsColor  = cast(immutable Color[5][])perms,
                   permsDrink  = cast(immutable Drink[5][])perms,
                   permsSmoke  = cast(immutable Smoke[5][])perms,
                   permsPet    = cast(immutable Pet[5][])perms;

  foreach (immutable ref number; permsNumber)
    if (isPossible(number.N))
      foreach (immutable ref color; permsColor)
        if (isPossible(number.N, color.N))
          foreach (immutable ref drink; permsDrink)
            if (isPossible(number.N, color.N, drink.N))
              foreach (immutable ref smoke; permsSmoke)
                if (isPossible(number.N, color.N, drink.N, smoke.N))
                  foreach (immutable ref pet; permsPet)
                    if (isPossible(number.N, color.N, drink.N, smoke.N,
                                   pet.N)) {
                      writeln("Found a solution:");
                      foreach (x; TypeTuple!(nation, number, color,
                                             drink, smoke, pet))
                        writefln("%6s: %12s%12s%12s%12s%12s",
                                 (Unqual!(typeof(x[0]))).stringof,
                                 x[0], x[1], x[2], x[3], x[4]);
                      writeln;
                  }
}
