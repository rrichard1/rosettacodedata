import Control.Monad.ST
import Control.Monad
import Data.Array.ST

lis :: Ord a => [a] -> [a]
lis lst = runST $ do
    pileTops <- newSTArray (1, length lst) []
    let bsearchPiles x len = aux 1 len where
          aux lo hi | lo > hi = return lo
                    | otherwise = do
            let mid = (lo + hi) `div` 2
            m <- readArray pileTops mid
            if head m < x then
              aux (mid+1) hi
            else
              aux lo (mid-1)
        f len x = do
          i <- bsearchPiles x len
          writeArray pileTops i . (x:) =<< if i == 1 then
                                             return []
                                           else
                                             readArray pileTops (i-1)
          return $ if i == len+1 then len+1 else len
    len <- foldM f 0 lst
    return . reverse =<< readArray pileTops len
    where newSTArray :: Ix i => (i,i) -> e -> ST s (STArray s i e)
          newSTArray = newArray

main :: IO ()
main = do
  print $ lis [3, 2, 6, 4, 5, 1]
  print $ lis [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
