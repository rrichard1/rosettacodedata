(let [[a m A M n z N Z] (map int [\a \m \A \M \n \z \N \Z])]
  (defn rot-13 [^Character c]
    (let [i (int c)]
      (cond
        (or (and (>= i a) (<= i m)) (and (>= i A) (<= i M)))
          (char (+ i 13))
        (or (and (>= i n) (<= i z)) (and (>= i N) (<= i Z)))
          (char (- i 13))
        :else c))))

; Elapsed time: 0.324057 msecs
(apply str (map rot-13 "The Quick Brown Fox Jumped Over The Lazy Dog!"))
