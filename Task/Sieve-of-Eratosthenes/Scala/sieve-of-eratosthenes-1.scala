def sieveOfEratosthenes(nTo: Int) = {
  val primes = collection.mutable.BitSet.empty.par ++ (2 to nTo)
  for {
    candidate <- 2 to Math.sqrt(nTo).toInt
    if primes contains candidate
  } primes --= candidate * candidate to nTo by candidate
  primes
}
// BitSet toList is shuffled when using the ParSet version.
println(sieveOfEratosthenes(100).toList.sorted)
println(sieveOfEratosthenes(100000000).size)
