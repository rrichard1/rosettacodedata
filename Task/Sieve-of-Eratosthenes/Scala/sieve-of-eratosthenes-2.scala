import scala.annotation.tailrec

  def sieveOfEratosthenes(nTo: Int) = {
    val nonprimes: Array[Int] = new Array((nTo >>> 5) + 1)

    def cullp(p: Int) = {
      @tailrec def cull(c: Int): Unit = {
        if (c <= nTo) nonprimes(c >>> 5) |= 1 << (c & 31)
        cull(c + p)
      }
      cull(p * p)
    }

    def nextprime(i: Int): Int = {
      if (i > nTo) i else if ((nonprimes(i >>> 5) & (1 << (i & 31))) != 0) nextprime(i + 1) else i
    }

    for {
      p <- 2 to Math.sqrt(nTo).toInt
      if (nonprimes(p >>> 5).&(1 << (p & 31))) == 0
    } cullp(p)

    Iterator.iterate(2)(c => nextprime(c + 1)).takeWhile(_ <= nTo)
}
