static void RC_array_init(int[] array, int num, int seed) {
  final int LCG_A = 1664525, LCG_C = 1013904223;
  for (int j = 0; j < num; j++) {
    seed = (LCG_A * seed + LCG_C) & 0x7fffffff;
    array[j] = seed;
  }
}
