let rc_init_array num seed =
    let rec rc acc num seed =
        match num with
          | 0 -> List.rev acc
          | i -> let seed' = (seed * 1664525 + 1013904223) &&& 0x7fffffff
                 rc (seed' :: acc) (num - 1) seed'
    rc [] num seed

do
    Seq.iter (fun x -> Printf.printf "%d\n" x) (rc_init_array 1000000 100)
