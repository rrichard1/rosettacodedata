import std.stdio, std.string, std.algorithm, std.array, std.conv,
       std.ascii, std.range;

string[] naturalSort(string[] arr) {
    static struct Part {
        string s;
        bool isNumber = false;
        ulong n;

        int opCmp(in ref Part other) const pure {
            return (isNumber && other.isNumber) ?
                   cmp([n], [other.n]) : cmp(s, other.s);
        }
    }

    static Part[] mapper(in string s) pure {
        // groupBy!isDigit could shorten this function.
        enum Kind { nothing, digit, notDigit }

        auto lk = Kind.nothing; // Last Kind.
        string[] parts;
        foreach (c; s.strip.tr(whitespace, " ", "s").toLower) {
            if (lk != Kind.nothing && c.isDigit == (lk == Kind.digit))
                parts.back ~= c;
            else
                parts ~= [c];
            lk = c.isDigit ? Kind.digit : Kind.notDigit;
        }

        auto r = parts.map!(p => p[0].isDigit ?
                            Part(p, true, p.to!ulong) :
                            Part(p)).array;
        return (r.length > 1 && r[0].s == "the") ? r.dropOne : r;
    }

    return arr.schwartzSort!mapper.release;
}

void main() {
    auto tests = [
    // Ignoring leading spaces.
    ["ignore leading spaces: 2-2", " ignore leading spaces: 2-1", "
     ignore leading spaces: 2+1", "  ignore leading spaces: 2+0"],

    // Ignoring multiple adjacent spaces (m.a.s).
    ["ignore m.a.s spaces: 2-2", "ignore m.a.s  spaces: 2-1",
     "ignore m.a.s   spaces: 2+0", "ignore m.a.s    spaces: 2+1"],

    // Equivalent whitespace characters.
    ["Equiv. spaces: 3-3", "Equiv.\rspaces: 3-2",
     "Equiv.\x0cspaces: 3-1", "Equiv.\x0bspaces: 3+0",
     "Equiv.\nspaces: 3+1", "Equiv.\tspaces: 3+2"],

    // Case Indepenent sort.
    ["cASE INDEPENENT: 3-2", "caSE INDEPENENT: 3-1",
     "casE INDEPENENT: 3+0", "case INDEPENENT: 3+1"],

    // Numeric fields as numerics.
    ["foo100bar99baz0.txt", "foo100bar10baz0.txt",
     "foo1000bar99baz10.txt", "foo1000bar99baz9.txt"],

    // Title sorts.
    ["The Wind in the Willows", "The 40th step more",
     "The 39 steps", "Wanda"]];

    foreach (test; tests) {
        test.writeln;
        writeln(test.naturalSort, "\n");
    }
}
