namespace RosettaCode {
    class Program {
        static void Main(string[] args) {
            BinaryTree<U> b = new BinaryTree<int>(6);
            b.left = new BinaryTree<int>(5);
            b.right = new BinaryTree<int>(7);
            BinaryTree<U> b2 = b.Map(x => x * 10);
        }
    }
}
