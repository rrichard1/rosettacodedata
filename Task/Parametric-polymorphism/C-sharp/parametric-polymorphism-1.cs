namespace RosettaCode {
    class BinaryTree<T> {
        public T value;
        public BinaryTree<T> left;
        public BinaryTree<T> right;

        public BinaryTree(T value) {
            this.value = value;
        }

        public BinaryTree<U> Map<U>(Func<T,U> f) {
            BinaryTree<U> Tree = new BinaryTree<U>(f(this.value));
            if (left != null) {
                Tree.left = left.Map(f);
            }
            if (right != null) {
                Tree.right = right.Map(f);
            }
            return Tree;
        }
    }
}
